package com.phantomastray.extensions.annotation;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.Set;

@SupportedAnnotationTypes("org.junit.Test")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class TestFinder extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        System.out.println("------------------------------------------------------------------------");
        System.out.println("@TEST annotation processor");
        System.out.println("------------------------------------------------------------------------");

        for(TypeElement annotation: annotations) {

            System.out.println(String.format("Found %d occurrences of annotation ", roundEnv.getElementsAnnotatedWith(annotation).size()));

            for(Element element: roundEnv.getElementsAnnotatedWith(annotation)) {

                System.out.println(String.format("Found in %s %s on %s %s", element.getEnclosingElement().getKind(), element.getEnclosingElement().getSimpleName(), element.getKind(), element.getSimpleName()));
                processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, String.format("Class: [[ %s ]] ", element.getSimpleName().toString()));
            }
        }

        return false;
    }
}
